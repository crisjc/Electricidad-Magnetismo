##
### Componente Practico del capitulo 1 (Carga Electrica y Campo Electrico)
### 

***Descripcion:***
>En esta secci encontraras todos los ejerccios correspondinete al [capitulo 1][1]

* [Ejercicios en clase](#id1)
* [Ejercicios en casa](#id2)
* [Ejercicios pruebas](#id3)

## Ejercicios en clase<a name="id1"></a>

>1. Calcule el campo electrico en el centro de la figura considere que $q=1x10^-8[C]$ y el lado del cuadrado es $a=5 [cm]$

***DATOS:***

$$ q=1x10^-8[C] $$
 
$a=5 [cm]$

***SOLUCION:***

## Ejercicios en casa<a name="id1"></a>
## Ejercicios en pruebas<a name="id1"></a>

   [1]: https://gitlab.com/crisjc/Electricidad-Magnetismo/blob/master/README.md
   [2]: http://another.url "A funky title"  
   
   
   
   