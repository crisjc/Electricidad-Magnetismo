# CONTENIDO
_____

Electromagnetismo
  
1. [Carga Electrica y Campo Electrico](#id1)
2. [Ley de Gauss](#id2)
3. [Potencial Electrico](#id3)
4. [Capacitancia y Dielectricos](#id4)
5. [Corriente Resistencia y fuerza Electromotriz](#id5)
6. [Circuitos de Corriente Directa](#id6)
7. [Campo Magnetico y Fuerzas Magneticas](#id7)
8. [Fuentes de Campo Magnetico](#id8)
9. [Induccion Electromagnetica](#id9)
10. [Inductancia](#id10)
11. [Formulario](#id11)
## Recursos 

[Link dirve nuevo](https://drive.google.com/drive/folders/1hVWtf8WFAIf1EVFfLV3a9fcAuL-8RrWZ?usp=sharing)

[Editor Dibujos](https://sketch.io/sketchpad/)

[Editor de formulas](http://www.wiris.com/editor/demo/es/)

[Calculo multivariable resumen](pdf/resumen-calculo.pdf)

[Enlace Dropbox](https://www.dropbox.com/sh/pb14mmnmk6if1bu/AABqz4KVfP8Icsyk3M4YMKGLa?dl=0)

[Enlace Google Drive](https://drive.google.com/drive/folders/1NRwFs0RZ-zFUcUV-MNDjoAFZYuJBiA-R?usp=sharing)

## Carga Electrica y Campo Electricoo<a name="id1"></a>

1. [Resumen](https://www.u-cursos.cl/usuario/42103e5ee2ce7442a3921d69b0200c93/mi_blog/r/Fisica_General_-_Fisica_Universitaria_Vol_2__ed_12%28Sears-Zemansky%29.pdf#page=57)

### 1.7 Dipolo electrico  
>  Un dipolo eléctrico consiste en un par de cargas eléctricas de igual magnitud ***q*** pero signo contrario, separadas por una distancia ***d***. Por definición el momento dipolar eléctrico $$p(vector)$$ tiene de magnitud $$p=qd$$. La dirección de $$p(vector)$$ va de la carga negativa ala carga positiva. Un dipolo eléctrico es un campo eléctrico $E(vector)$ que experimenta un par de torsión denominado torque $$T(vector)$$ igual al producto vectorial de $p y E(vectores)$. La magnitud del torque o par de torsión depende del Angulo θ entre $$py E(vectores)$$. La energía potencial $U$ para un dipolo eléctrico en un campo eléctrico también depende de la orientación relativa de $$p y E(vectores)$$ ejercidos 21.14 y 21.15

***formulas de dipolo electrico***

![formulas](imagenes/dipolo.png)
#### Dipolo Electrico

![formulas](imagenes/dipolo1.png)

***Momento dipolar electrico analisis Algebraico***

![formulas](imagenes/momento-dipolar.png)

* El vector unitario que va de positiva a la negativa
* La carga $q$ es el modulo de una carga 

**Campo creado por el dipolo electrico**
<math xmlns="http://www.w3.org/1998/Math/MathML"><mi>V</mi><mo>(</mo><mi>p</mi><mi>o</mi><mi>t</mi><mi>e</mi><mi>n</mi><mi>c</mi><mi>i</mi><mi>a</mi><mi>l</mi><mo>)</mo></math>

***Formula del campo electrico creado por un dipolo electrico***

![dipolo](imagenes/campo-electrico-dipolo-potencial.PNG)

* Calculamos el potencial electrico creado por un dipolo electrico

>Calculo

* Sacar el gradiente para finalmente obtener el campo

1. Cae inversamente a la distancia al cuadrado.

2. Para moleculas dipolares caen muy rapido como a la distancia al cuadrado, el agua es dipolar y se anula porque cae mas rapido 

>Calculo de Gradiente

* Gradiente en diferentes codernadas

![formulas](imagenes/gradiente-cordenadas.PNG)

* fuerza sobre un dipolo electrico en un campo externo

***Molecula de agua***
![formulas](imagenes/molecula-agua.png)

>**Como EL dipolo reacciona a un campo externo**
> Va a rotar depende del signo. 
> Y si se aleja  o se atrae depende  del sistema de referencia.

~~~
la fuerza que acuta sobre dopolo electrico en un campo electrico  que no es uniforme(no es constante) no es cero.
~~~ 

~~~
la fuerza de un dipolo  en un campo electrico uniforme es 0 y va a comenzar a rotar
~~~

![grafica enrgia de un dipolo](imagenes/grafica-dipolo-energia.png)

## Ley de Gauss<a name="id2"></a>

### ley de Gauss

#### tips

***formulas de ley de gauss***
![formulas](imagenes/flujo-electrico.png)

> Si el flujo es negativo o positivo lo determina la dirección del campo ***E*** y el unitario de la superficie ***dA*** siempre este es positivo.

> La superficie es arbitraria para calcular el campo por la eliminación de la dependencia entre sus radio es por que que cualquier superficie cerrada puede determinar el campo.

>El flujo es independiente del radio de la esfera.
> Se pueden invertir los limites de cualquier integral para poder cambiar el signo 

>Una molecula esta en su estado estable cuando el flujo de campo electrico es cero o todas las lineas de campo que salen entran pero cuando se produce un proceso de inonizacion es puede aquirir una carga total neta positiva o negativa 

> **<math xmlns="http://www.w3.org/1998/Math/MathML"><mo>&#x2207;</mo></math>** denominado un operador vectorial.
> Ejemplo operador vectoria napla **cartesiano**
> $D/Dx*i+D/Dy*j+D/Dz*k$



**Rotacional**
><math xmlns="http://www.w3.org/1998/Math/MathML"><mo>&#x2207;</mo><mi>X</mi><mo>&#xA0;</mo><mi>E</mi></math>

**Gradiente**
><math xmlns="http://www.w3.org/1998/Math/MathML"><mo>&#x2207;</mo><mi>&#x3C6;</mi></math>

**Divergencia**
><math xmlns="http://www.w3.org/1998/Math/MathML"><mo>&#x2207;</mo><mo>.</mo><mi>E</mi></math>

>nos permite calcular cuanta carga tenemos encerrada

**Densidad de carga**
![densidad de carga](imagenes/densidad-de-carga.png)

* El campo electrico diverge de cargas positivas a cargas negativas
* Los sumideros o fuentes son las cargas electricas

**Trabajo realizado en toda superfice cerrada conservativa es *0***
![formula-trabajo](imagenes/trabajo-W-formula.png)

 * Circulacion de campo electico = 0
 * El flujo es <math xmlns="http://www.w3.org/1998/Math/MathML"><mo>&#x2260;</mo></math> de **0** el campo electrico es potencial
  
  
**Utilizar la ley de gauss de dos maneras**(conclusión)
![gauss](imagenes/ley-gauss-carga-encerada.png)
* Determinar el campo electrico si se conoce la distribucion de carga
* Determinar la carga si se conoce el  campo electrico 
  


[Relacion de campo electrico con la densidad de carga](http://hyperphysics.phy-astr.gsu.edu/hbasees/electric/diverg.html)
1. [Resumen](https://www.u-cursos.cl/usuario/42103e5ee2ce7442a3921d69b0200c93/mi_blog/r/Fisica_General_-_Fisica_Universitaria_Vol_2__ed_12%28Sears-Zemansky%29.pdf#page=90)

## Potencial Electric<a name="id3"></a>

1. [Resumen](https://www.u-cursos.cl/usuario/42103e5ee2ce7442a3921d69b0200c93/mi_blog/r/Fisica_General_-_Fisica_Universitaria_Vol_2__ed_12%28Sears-Zemansky%29.pdf#page=122)

## Capacitancia y Dielectricos<a name="id4"></a>

1. [Resumen](https://www.u-cursos.cl/usuario/42103e5ee2ce7442a3921d69b0200c93/mi_blog/r/Fisica_General_-_Fisica_Universitaria_Vol_2__ed_12%28Sears-Zemansky%29.pdf#page=155)

### Cargas electroestaticas en el medio 

**Dielectrico:** Sus electrones estan atrapados en el nucleo.

**Capacitor:** Dos conductores a una distancia X almacena carga electrica por que genera un campo electrico.

* Es una medida de la cantidad de energia electrica que puede almacenas el capacitor.

**Capacitor de placas paralelas**

(demostracion capacitor)

![capacitor](imagenes/demostracion-capacitor.PNG)

[capacitor fuente](http://www.sc.ehu.es/sbweb/fisica/elecmagnet/campo_electrico/plano/plano.htm)

## Corriente Resistencia y fuerza Electromotriz<a name="id5"></a>

1. [Resumen](https://www.u-cursos.cl/usuario/42103e5ee2ce7442a3921d69b0200c93/mi_blog/r/Fisica_General_-_Fisica_Universitaria_Vol_2__ed_12%28Sears-Zemansky%29.pdf#page=189)

## Circuitos de Corriente Directa<a name="id6"></a>

1. [Capitulo 21](https://www.u-cursos.cl/usuario/42103e5ee2ce7442a3921d69b0200c93/mi_blog/r/Fisica_General_-_Fisica_Universitaria_Vol_2__ed_12%28Sears-Zemansky%29.pdf#page=223)

## Campo Magnetico y Fuerzas Magneticas<a name="id7"></a>

1. [Capitulo 21](https://www.u-cursos.cl/usuario/42103e5ee2ce7442a3921d69b0200c93/mi_blog/r/Fisica_General_-_Fisica_Universitaria_Vol_2__ed_12%28Sears-Zemansky%29.pdf#page=263)

## Fuentes de Campo Magnetico<a name="id8"></a>

1. [Capitulo 21](https://www.u-cursos.cl/usuario/42103e5ee2ce7442a3921d69b0200c93/mi_blog/r/Fisica_General_-_Fisica_Universitaria_Vol_2__ed_12%28Sears-Zemansky%29.pdf#page=300)

## Induccion Electromagnetica<a name="id9"></a>

1. [Capitulo 21](https://www.u-cursos.cl/usuario/42103e5ee2ce7442a3921d69b0200c93/mi_blog/r/Fisica_General_-_Fisica_Universitaria_Vol_2__ed_12%28Sears-Zemansky%29.pdf#page=337)

## Inductancia<a name="id10"></a>

1. [Capitulo 21](https://www.u-cursos.cl/usuario/42103e5ee2ce7442a3921d69b0200c93/mi_blog/r/Fisica_General_-_Fisica_Universitaria_Vol_2__ed_12%28Sears-Zemansky%29.pdf#page=370)


## Formulario <a name="id10"></a>

**Constantes imporntantes**

