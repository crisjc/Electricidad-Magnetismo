###
### Capacitancia y capacitores evaluacion para practica de capacitores 

***Evaluacion Previa***

**1. Seleccione dos aplicaciones importantes que tiene el capacitor.**

  Seleccione una o más de una:
* [x]  a. Circuitos Sintonizados de los receptores de radio y Televisión

* [x]  b. Maquina Z

* [ ]  c. Impresora Láser

**2. La carga neta almacenada en las dos placas de un capacitor es:**

Seleccione una:
* [ ]  a.	2 veces la carga de cada conductor

* [x]  b.	Cero.
* [ ]  c.	Necesita conocer el potencial eléctrico para conocer la carga.

**3. Completar cada una de las oraciones que describen los procesos físicos que ocurren durante la carga y descarga de un capacitor:**

**La respuesta correcta es: "Cuando el capacitor se descarga, la energía almacenada es_____________". –** recuperada en forma de trabajo realizado por el campo eléctrico.", **"La energía potencial eléctrica almacenada en un capacitor cargado es ____________". –** exactamente igual al trabajo requerido para mover cargas opuestas y colocarlas en los diferentes conductores., **" Las diferencias de potencial v varían desde V hasta cero cuando es____________". –** descargado el capacitor."

**4. Los procesos físicos que ocurren en un capacitor se los puede comparar con aquellos que ocurren en un resorte estirado. En esta analogía, escoja para cada propiedad eléctrica su correspondiente mecánico:**

Capacitor cargado : ***resorte estirado***

Carga Eléctrica : ***Distancia de elongacion***

Recíproco de Capacitancia : ***constante de fuerza K***

**5. La capacitancia de un capacitor de placas paralelas es C. Si se duplica el área de las placas y la separación de las mismas, el valor de la nueva capacitancia es:**

Seleccione una:
* [ ]  a.	2C

* [ ]  b.	C/2

* [x]  c.	C
* [ ]  d.	Ninguna de las respuestas propuestas

**6. Se tiene un capacitor de placas paralelas de supercie A y una distancia entre ellas
d. Si se aumenta o disminuye progresivamente la distancia d, el resultado de la gráfica de capacitancia en función de 1/d es una:**

Seleccione una:

* [ ]  a.	relación inversa

* [ ]  b.	parábola

* [x]  c.	recta
* [ ]  d.	exponencial

**7. Un capacitor almacena una carga Q con una diferencia de potencial V, ¿Qué pasaría si el voltaje que suministra la batería al capacitor se duplica a 2V?**

Seleccione una:

* [ ]  a.	Tanto la capacitancia como la carga disminuyen hasta la mitad de sus valores iniciales

* [x]  b.	La capacitancia permanece igual pero la carga se duplica

* [ ]  c.	La capacitancia disminuye hasta la mitad de su valor inicial y la carga se mantiene igual

* [ ]  d.	Tanto la capacitancia como la carga se duplican

**8. Para establecer la energía almacenada en un capacitor considerando el campo eléctrico se utiliza una nueva definición que es:**

Seleccione una:

* [ ]  a. Densidad de Carga


* [ ]  b. Densidad de campo Eléctrico


* [x]  c. Densidad de energía

**9. La capacitancia de un capacitor de placas paralelas:**

Seleccione una:

* [x]  a.	Depende solo de la geometría del capacitor, es decir del área de las placas y la separación entre estas.

* [ ]  b.	Depende de la carga almacenada en cada placa

* [ ]  c.	Depende del voltaje aplicado a cada placa

* [ ]  d.	Depende de el área de las placas, la separación entre estas, el voltaje y la carga almacenada.

**10. La energía almacenada en un capacitor es:
(Se puede deducir calculando el trabajo por unidad de carga realizado para llevar las cargas a las placas del capacitor)**

![ejercicio 21.15](../imagenes/energia-capacitor.PNG)

**11 En un capacitor de placas planas, ¿Cuándo es  posible considerar al campo eléctrico entre las placas como uniforme?**

Seleccione una:

* [x]  a.	Cuando la distancia entre las placas es pequeña en comparación a las dimensiones de las placas

* [ ]  b.	Cuando la distancia entre las placas es mayor que las dimensiones de las placas

* [ ]  c.	Cuando la distancia entre las placas es igual a las dimensiones de las placas

**12. Seleccione la opción correcta:**

* [ ]  a.	En una combinación en paralelo de capacitores las diferencias de potencial individuales son diferentes

* [ ]  b.	En una combinación en serie, la carga total es la suma de las cargas en capacitores individuales

* [x]  c.	La capacitancia equivalente en una combinación es paralelo de capacitores es la suma algebraica de las capacitancias individuales

**13.Un capacitor tiene vacío en el espacio entre los conductores. Si se duplica la cantidad de carga en cada conductor, ¿qué pasa con la capacitancia?**

Seleccione una:

* [x]  a. La capacitancia sólo depende de la geometría del capacitor Correcta

* [ ]  b. No se puede determinar si no se conoce el potencial aplicado entre los conductores.

* [ ]  c. Aumenta

* [ ]  d. Disminuye

**14. La unidad de la capacitancia en el SI es:**

Seleccione una:

* [ ]  a. F/N

* [x]  b. C/V o F Correcta

* [ ]  c. C

**15. El capacitor es un elemento, que en un circuito eléctrico es utilizado para:**

Seleccione una:

a. disipar energía

* [x]  b. almacenar energía Correcta

**16.Suponga que las placas de un capacitor no son iguales, es decir tienen diferentes áreas. Cuando el capacitor se carga conectándolo a una batería, las cargas en las dos placas tienen magnitud:**

Seleccione una:

* [ ]  a. diferente Incorrecta

* [x]  b. igual


**17. Un capacitor de placas paralelas está conectado a una fuente de voltaje constante. Si se separan las placas del capacitor,**

Seleccione una o más de una:

* [ ]  a. El campo disminuye pero la carga aumenta

* [x]  b. Se reduce la carga almacenada Correcta

* [ ]  c. El campo aumenta y la carga disminuye.

* [ ]  d. El campo eléctrico y la carga almacenada aumentan

**18. ¿Cuál sería la superficie de un capacitor de placas paralelas de 1F de capacidad y una distancia entre placas de 1mm?**

Seleccione una:

* [ ]  a. Es razonable puede ser visto en dispositivos electrónicos.

* [ ]  b. Hace falta conocer la carga y el potencial eléctrico.

* [x]  c. Aproximadamente 113 Km2 (mayor a la superficie de Cuenca) 

**19. Seleccione la ecuación de la capacitancia para los diferentes tipos de capacitores.**

![formulas capacitancia](../imagenes/formulas-capacitancia.PNG)

**20. La suceptibilidad eléctrica χ es:**

Seleccione una o más de una:

* [ ]  a. Determina cuantas veces la velocidad de la luz en el vacio es mayor que en el medio.
* [x]  b. Es un tensor que caracteriza la capacidad de un dielectrico para polalizarce en presencia del campo electrico.
* [ ]  c. X Puede tomar valores positivos y negativos.

**21. Una plancha infinita de material aislante cuya constante dieléctrica K y permitividad ε=Kε0 se encuentra en un campo electro uniforme de magnitud E0. El campo es perpendicular a la superficie del material como se muestra en la figura. La magnitud del campo eléctrico dentro de la plancha es:**

![campo inducido](../imagenes/campo-inducido.png)

**22. Tres capacitores se conectan como se muestra en la figura. Las brechas entre las platcas de los tres capacitores se llenan con aire (k=1.0), las capacitancias se indican en la figura. Una diferencia de potencial constante de 6V se mantiene entre los puntos A y B de un circuito.**

![tres capacitores](../imagenes/tres-capacitores.png)

**Depués, un material dieléctrico con k=5.4 se inserta entre los platos del capacitor C2. ¿Cuál es el cambio de la energía almacenada en ese capacitor?**

![tres capacitores](../imagenes/tres-capacitores-resp.png)

**23. En el circuito mostrado, los capacitores tienen la misma capacitancia. Suponga que en un primer momento el switch S2 está abierto mientras que el switch S1 está cerrado. Después de mucho tiempo se abre el switch S1 y se cierra el switch S2. Determine la diferencia de potencial en el capacitor C4 si la V1=6,7 V.**

![tres capacitores](../imagenes/capacitores-interuptores.png)

**Nota:**
* Voltaje divido para 6 

**24. Un capacitor de placas paralelas con un área de A=4,1[cm2] y una separación entre sus placas de d=5[mm], se encuentra cargado por lo cual induce una densidad de carga de magnitud σind=8,7[Cm2] sobre una lámina de porcelana colocada entre las placas de este.
¿Cuál es la carga almacenada en el capacitor Q en [mC]?

Considerar κ=6 para la porcelana.**

**Nota:**
* La  formula de la densidad de carga inducida

![densidad inducida](../imagenes/densidad-inducida.png)

<math xmlns="http://www.w3.org/1998/Math/MathML"><msub><mi>&#x3C3;</mi></msub><mo>=</mo><mi>Q/A
de [cm^2] --> [m^2] = 1e-4


