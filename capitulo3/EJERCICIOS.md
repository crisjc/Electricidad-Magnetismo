##
### Componente Practico del capitulo 1 (Potencial Electrico)
### 

***Descripcion:***
>En esta secci encontraras todos los ejerccios correspondinete al [capitulo 3][1]

* [Ejercicios en clase](#id1)
* [Ejercicios en casa](#id2)
* [Ejercicios pruebas](#id3)

## Ejercicios en clase<a name="id1"></a>


## Ejercicios en casa<a name="id1"></a>

>23.15 Una partícula pequeña tiene carga de 25.00 mC y masa de  kg. Se desplaza desde el punto A, donde el potencial eléctrico es VA = 200 V, al punto B, donde el potencial eléctrico es VB =800V. La fuerza eléctrica es la única que actúa sobre la partícula, la cual tiene una rapidez de 5.00 m>s en el punto A. ¿Cuál es su rapidez en el punto B? ¿Se mueve más rápido o más lento en B que en A? Explique su respuesta.

![ejercicio 21.15](../imagenes/ejercicio-21-15.png)

## Ejercicios en pruebas<a name="id1"></a>

   [1]: https://gitlab.com/crisjc/Electricidad-Magnetismo/blob/master/README.md
   [2]: http://another.url "A funky title"  